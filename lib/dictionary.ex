defmodule Dictionary do
  @moduledoc ~S"""
  Provides a random word from a predefined dictionary.

  ## Example
      iex> a_word = Dictionary.random_word()
  """

  alias Dictionary.WordList

  @doc """
  Provide a random word.
  """
  @spec random_word :: binary
  defdelegate random_word(), to: WordList
end
