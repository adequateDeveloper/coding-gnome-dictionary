defmodule Dictionary.WordList do
  @moduledoc ~S"""
  Provides a random word from a predefined dictionary which is managed by an Agent
  """

  @compile if Mix.env() == :test, do: :export_all

  @name __MODULE__

  @doc """
  Starts an Agent to maintain a list of words
  """
  @spec start_link :: {:ok, pid}
  def start_link do
    # note - the function to build the list of words is executed in the Agent (server)
    Agent.start_link(&load_words/0, name: @name)
  end

  @doc """
  Provide a random word from a list of words
  """
  @spec random_word :: String.t()
  def random_word do
    # cause Agent to blow up 1 out of 3 times - for Supervisor testing
    #    if :rand.uniform < 0.33 do
    #      Agent.get(@name, fn _it -> exit(:boom) end)
    #    end

    # note - the function to retrieve a word is executed in the Agent (server)
    Agent.get(@name, &Enum.random/1)
  end

  # Load a file of words and chunk them into a list
  defp load_words do
    # __DIR__ is a compile-time macro, and /assets is not the expected folder for non-code resources.
    # see: https://elixirforum.com/t/including-data-files-in-a-distillery-release/2813

    # "../../assets/words.txt"
    # |> Path.expand(__DIR__)

    Application.app_dir(:dictionary, "/priv/words.txt")
    |> File.read!()
    |> String.split(~r/\n/)
  end
end
