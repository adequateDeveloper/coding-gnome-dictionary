defmodule Dictionary.Application do
  @moduledoc false

  use Application

  alias Dictionary.WordList

  def start(_type, _args) do
    children = [
      # the WordList child_spec that utilizes a non-default start_link/0
      %{id: WordList, start: {WordList, :start_link, []}}
    ]

    options = [
      name: Dictionary.Supervisor,
      strategy: :one_for_one
    ]

    Supervisor.start_link(children, options)
  end
end
