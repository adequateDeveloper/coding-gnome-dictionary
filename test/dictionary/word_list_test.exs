defmodule Dictionary.WordListTest do
  use ExUnit.Case, async: true
  #  doctest WordList

  alias Dictionary.WordList

  test "WordList is automatically loaded and random_word/0 returns a word" do
    refute is_nil(WordList.random_word())
    assert is_binary(WordList.random_word())
    assert String.length(WordList.random_word()) > 0
  end

  test "load_words/0 returns a word list" do
    words = WordList.load_words()

    assert is_list(words)
    assert length(words) > 0
  end
end
