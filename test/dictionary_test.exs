defmodule DictionaryTest do
  use ExUnit.Case, async: true
  # doctest Dictionary  # test below fails if this line enabled

  test "Dictionary is automatically loaded and random_word/0 returns a word" do
    refute is_nil(Dictionary.random_word())
    assert is_binary(Dictionary.random_word())
    assert String.length(Dictionary.random_word()) > 0
  end
end
